<?php

namespace Drupal\social_lms_integrator_export\Plugin\SocialLMSIntegratorExportPlugin;

use Drupal\social_lms_integrator_export\Plugin\SocialLMSIntegratorExportPluginBase;
use Drupal\social_lms_integrator_enrollment\IterationEnrollmentInterface;

/**
 * Provides a 'IterationEnrollmentAddressPostalCode' Social LMS Integrator export row.
 *
 * @SocialLMSIntegratorExportPlugin(
 *  id = "iteration_enrollment_address_postal_code",
 *  label = @Translation("Postal code"),
 *  weight = -480,
 * )
 */
class IterationEnrollmentAddressPostalCode extends SocialLMSIntegratorExportPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getHeader() {
    return $this->t('Postal code');
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(IterationEnrollmentInterface $entity) {
    // Get the user from the Iteration enrollment
    $user = $this->getAccount($entity);
    return $this->profileGetAddressFieldValue('field_profile_address', 'postal_code', $this->getProfile($user));
  }

}