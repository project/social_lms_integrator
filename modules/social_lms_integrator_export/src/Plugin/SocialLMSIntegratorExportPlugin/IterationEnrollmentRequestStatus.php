<?php

namespace Drupal\social_lms_integrator_export\Plugin\SocialLMSIntegratorExportPlugin;

use Drupal\social_lms_integrator_export\Plugin\SocialLMSIntegratorExportPluginBase;
use Drupal\social_lms_integrator_enrollment\IterationEnrollmentInterface;

/**
 * Provides a 'IterationEnrollmentRequestStatus' Social LMS Integrator export row.
 *
 * @SocialLMSIntegratorExportPlugin(
 *  id = "iteration_enrollment_request_status",
 *  label = @Translation("Request status"),
 *  weight = -460,
 * )
 */
class IterationEnrollmentRequestStatus extends SocialLMSIntegratorExportPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getHeader() {
    return $this->t('Request status');
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(IterationEnrollmentInterface $entity) {
    return $entity->field_request_or_invite_status->value;
  }


}