<?php

namespace Drupal\social_lms_integrator_export\Plugin\SocialLMSIntegratorExportPlugin;

use Drupal\social_lms_integrator_export\Plugin\SocialLMSIntegratorExportPluginBase;
use Drupal\social_lms_integrator_enrollment\IterationEnrollmentInterface;

/**
 * Provides a 'IterationEnrollmentAddressAdministrative' Social LMS Integrator export row.
 *
 * @SocialLMSIntegratorExportPlugin(
 *  id = "iteration_enrollment_address_administrative",
 *  label = @Translation("Administrative address"),
 *  weight = -480,
 * )
 */
class IterationEnrollmentAddressAdministrative extends SocialLMSIntegratorExportPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getHeader() {
    return $this->t('Administrative address');
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(IterationEnrollmentInterface $entity) {
    // Get the user from the Iteration enrollment
    $user = $this->getAccount($entity);
    return $this->profileGetAddressFieldValue('field_profile_address', 'administrative_area', $this->getProfile($user));
  }

}