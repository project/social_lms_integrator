<?php

namespace Drupal\social_lms_integrator_export\Plugin\SocialLMSIntegratorExportPlugin;

use Drupal\social_lms_integrator_export\Plugin\SocialLMSIntegratorExportPluginBase;
use Drupal\social_lms_integrator_enrollment\IterationEnrollmentInterface;

/**
 * Provides a 'IterationEnrollmentAddressLine2' Social LMS Integrator export row.
 *
 * @SocialLMSIntegratorExportPlugin(
 *  id = "iteration_enrollment_address_line2",
 *  label = @Translation("Address line 2"),
 *  weight = -480,
 * )
 */
class IterationEnrollmentAddressLine2 extends SocialLMSIntegratorExportPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getHeader() {
    return $this->t('Address line 2');
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(IterationEnrollmentInterface $entity) {
    // Get the user from the Iteration enrollment
    $user = $this->getAccount($entity);
    return $this->profileGetAddressFieldValue('field_profile_address', 'address_line2', $this->getProfile($user));
  }

}