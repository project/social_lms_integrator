<?php

namespace Drupal\social_lms_integrator_export\Plugin\SocialLMSIntegratorExportPlugin;

use Drupal\social_lms_integrator_export\Plugin\SocialLMSIntegratorExportPluginBase;
use Drupal\social_lms_integrator_enrollment\IterationEnrollmentInterface;

/**
 * Provides a 'IterationEnrollmentRequestMessage' Social LMS Integrator export row.
 *
 * @SocialLMSIntegratorExportPlugin(
 *  id = "iteration_enrollment_request_message",
 *  label = @Translation("Message"),
 *  weight = -460,
 * )
 */
class IterationEnrollmentRequestMessage extends SocialLMSIntegratorExportPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getHeader() {
    return $this->t('Message');
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(IterationEnrollmentInterface $entity) {
    return $entity->field_request_message->value;
  }


}