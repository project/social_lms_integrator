<?php

namespace Drupal\social_lms_integrator_export\Plugin\SocialLMSIntegratorExportPlugin;

use Drupal\social_lms_integrator_export\Plugin\SocialLMSIntegratorExportPluginBase;
use Drupal\social_lms_integrator_enrollment\IterationEnrollmentInterface;

/**
 * Provides a 'IterationEnrollmentOrganization' Social LMS Integrator export row.
 *
 * @SocialLMSIntegratorExportPlugin(
 *  id = "iteration_enrollment_organization",
 *  label = @Translation("Organization"),
 *  weight = -320,
 * )
 */
class IterationEnrollmentOrganization extends SocialLMSIntegratorExportPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getHeader() {
    return $this->t('Organization');
  }

  /**
   * Returns the value.
   *
   * @param \Drupal\social_lms_integrator_enrollment\IterationEnrollmentInterface $entity
   *   The Iteration enrollment entity to get the value from.
   *
   * @return string
   *   The value.
   */
  public function getValue(IterationEnrollmentInterface $entity) {
    $user = $this->getAccount($entity);
    return $this->profileGetFieldValue('field_profile_organization', $this->getProfile($user));
  }

}
