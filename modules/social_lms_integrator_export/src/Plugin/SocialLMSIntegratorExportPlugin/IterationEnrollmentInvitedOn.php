<?php

namespace Drupal\social_lms_integrator_export\Plugin\SocialLMSIntegratorExportPlugin;

use Drupal\social_lms_integrator_export\Plugin\SocialLMSIntegratorExportPluginBase;
use Drupal\social_lms_integrator_enrollment\IterationEnrollmentInterface;

/**
 * Provides a 'IterationEnrollmentInvitedOn' Social LMS Integrator export row.
 *
 * @SocialLMSIntegratorExportPlugin(
 *  id = "iteration_enrollment_invited_on",
 *  label = @Translation("Invited on"),
 *  weight = -460,
 * )
 */
class IterationEnrollmentInvitedOn extends SocialLMSIntegratorExportPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getHeader() {
    return $this->t('Invited on');
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(IterationEnrollmentInterface $entity) {
    return $this->format($entity);
  }

  /**
   * Formats a date, using a date type or a custom date format string.
   *
   * @param mixed $entity
   *   The entity object.
   *
   * @return string
   *   A translated date string in the requested format. Since the format may
   *   contain user input, this value should be escaped when output.
   */
  public function format($entity) {
    return $this->dateFormatter->format($entity->getCreatedTime(), 'short');
  }

}