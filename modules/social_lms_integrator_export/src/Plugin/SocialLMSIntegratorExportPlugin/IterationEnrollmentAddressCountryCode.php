<?php

namespace Drupal\social_lms_integrator_export\Plugin\SocialLMSIntegratorExportPlugin;

use Drupal\social_lms_integrator_export\Plugin\SocialLMSIntegratorExportPluginBase;
use Drupal\social_lms_integrator_enrollment\IterationEnrollmentInterface;

/**
 * Provides a 'IterationEnrollmentAddressCountryCode' Social LMS Integrator export row.
 *
 * @SocialLMSIntegratorExportPlugin(
 *  id = "iteration_enrollment_address_country_code",
 *  label = @Translation("Country code"),
 *  weight = -480,
 * )
 */
class IterationEnrollmentAddressCountryCode extends SocialLMSIntegratorExportPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getHeader() {
    return $this->t('Country code');
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(IterationEnrollmentInterface $entity) {
    // Get the user from the Iteration enrollment
    $user = $this->getAccount($entity);
    return $this->profileGetAddressFieldValue('field_profile_address', 'country_code', $this->getProfile($user));
  }

}