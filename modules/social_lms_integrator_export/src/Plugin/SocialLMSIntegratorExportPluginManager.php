<?php

namespace Drupal\social_lms_integrator_export\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\social_lms_integrator_export\Annotation\SocialLMSIntegratorExportPlugin;

/**
 * Provides the Social LMS Integrator export plugin plugin manager.
 */
class SocialLMSIntegratorExportPluginManager extends DefaultPluginManager {

  /**
   * Constructs a new SocialLMSIntegratorExportPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/SocialLMSIntegratorExportPlugin', $namespaces, $module_handler, SocialLMSIntegratorExportPluginInterface::class, SocialLMSIntegratorExportPlugin::class);
    $this->alterInfo('social_lms_integrator_export_plugin_info');
    $this->setCacheBackend($cache_backend, 'social_lms_integrator_export_iteration_enrollment_export_plugin_plugins');
  }

}
