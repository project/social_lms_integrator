<?php

namespace Drupal\social_lms_integrator_export\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Social LMS Integrator export plugin item annotation object.
 *
 * @see \Drupal\social_lms_integrator_export\Plugin\SocialLMSIntegratorExportPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class SocialLMSIntegratorExportPlugin extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The plugin weight.
   *
   * @var int
   */
  public $weight;

}
