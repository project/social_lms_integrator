<?php

/**
 * @file
 * Builds placeholder replacement tokens for Social LMS Integrator Iteration module.
 */

use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\message\Entity\Message;
use Drupal\social_lms_integrator_enrollment\IterationEnrollmentInterface;

/**
 * Implements hook_token_info().
 */
function social_lms_integrator_iteration_token_info() {

  $type = [
    'name' => t('Social LMS Integrator Iteration'),
    'description' => t('Tokens from the Social LMS Integrator Iteration.'),
  ];

  $iteration['iteration_iam_organizing'] = [
    'name' => t('URL of the Iteration I organize.'),
    'description' => t('URL of the Iteration I organize.'),
  ];

  $iteration['enrolled_user'] = [
    'name' => t('Username of user enrolled to the iteration.'),
    'description' => t('Username of user enrolled to the Iteration. Can be an anonymous user.'),
  ];

  $iteration['enrolled_iteration_url'] = [
    'name' => t('Url of the Iteration a user enrolled to.'),
    'description' => t('Url of the Iteration a user enrolled to.'),
  ];

  return [
    'types' => ['social_lms_integrator_iteration' => $type],
    'tokens' => [
      'social_lms_integrator_iteration' => $iteration,
    ],
  ];
}

/**
 * Implements hook_tokens().
 */
function social_lms_integrator_iteration_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];
  if ($type === 'social_lms_integrator_iteration' && !empty($data['message'])) {
    /** @var Drupal\message\Entity\Message $message */
    $message = $data['message'];

    if ($message instanceof Message) {
      $iteration_enrollment_id = $message->getFieldValue('field_message_related_object', 'target_id');

      foreach ($tokens as $name => $original) {
        switch ($name) {
          case 'iteration_iam_organizing':
            if (!empty($iteration_enrollment_id)) {
              $link = _social_lms_integrator_iteration_get_link_to_iteration_from_enrollment($iteration_enrollment_id);
              if ($link !== NULL) {
                $replacements[$original] = $link;
              }
            }
            break;

          case 'enrolled_iteration_url':
            if (!empty($iteration_enrollment_id)) {
              $link = _social_lms_integrator_iteration_get_link_to_iteration_from_enrollment($iteration_enrollment_id, TRUE);
              if ($link !== NULL) {
                $replacements[$original] = $link;
              }
            }
            break;

          case 'enrolled_user':
            if (!empty($iteration_enrollment_id)) {
              $enrollment_name = _social_lms_integrator_iteration_get_name_from_enrollment($iteration_enrollment_id);

              if ($enrollment_name !== NULL) {
                $replacements[$original] = $enrollment_name;
              }
            }
            break;
        }
      }
    }
  }

  return $replacements;
}

/**
 * Implements hook_tokens_alter().
 */
function social_lms_integrator_iteration_tokens_alter(array &$replacements, array $context, BubbleableMetadata $bubbleable_metadata) {
  if ($context['type'] == 'message' && !empty($context['data']['message'])) {
    /** @var Drupal\message\Entity\Message $message */
    $message = $context['data']['message'];

    // Alter the [message:cta_button] token, and [message:cta_button]
    // and replace it with the rendered content with new text and link.
    if (isset($context['tokens']['cta_button'])|| isset($context['tokens']['preview'])) {
      if ($message->hasField('field_message_related_object') && !$message->get('field_message_related_object')->isEmpty()) {
        /** @var \Drupal\social_activity\EmailTokenServices $email_token_services */
        $email_token_services = \Drupal::service('social_activity.email_token_services');

        $target_type = $message->getFieldValue('field_message_related_object', 'target_type');

        if ($target_type === 'iteration_enrollment' && $iteration_enrollment_id = $message->getFieldValue('field_message_related_object', 'target_id')) {
          if (isset($context['tokens']['cta_button'])) {
            $iteration = _social_lms_integrator_iteration_get_iteration_from_enrollment($iteration_enrollment_id);
            if ($iteration !== NULL) {
              $message_template_id = $message->getTemplate()->id();
              switch ($message_template_id) {
                case 'activity_on_iterations_im_organizing':
                  $link = Url::fromRoute('iteration_manage_enrollments.page_manage_enrollments', ['node' => $iteration->id()]);
                  $cta_button = $email_token_services->getCtaButton($link, new TranslatableMarkup('View enrollments'));
                  $replacements[$context['tokens']['cta_button']] = \Drupal::service('renderer')
                    ->renderPlain($cta_button);
                  break;

                case 'request_iteration_enrollment':
                  $link = Url::fromRoute('view.iteration_manage_enrollment_requests.page_manage_enrollment_requests', ['node' => $iteration->id()]);
                  $cta_button = $email_token_services->getCtaButton($link, new TranslatableMarkup('View enrollment requests'));
                  $replacements[$context['tokens']['cta_button']] = \Drupal::service('renderer')
                    ->renderPlain($cta_button);
                  break;

                case 'member_added_by_it_organiser':
                case 'iteration_request_approved':
                  $link = $iteration->toUrl('canonical');
                  $cta_button = $email_token_services->getCtaButton($link, new TranslatableMarkup('See the iteration'));
                  $replacements[$context['tokens']['cta_button']] = \Drupal::service('renderer')
                    ->renderPlain($cta_button);
                  break;

                case 'user_was_enrolled_to_iteration':
                  $link = $iteration->toUrl();
                  $cta_button = $email_token_services->getCtaButton($link, new TranslatableMarkup('See iteration details'));
                  $replacements[$context['tokens']['cta_button']] = \Drupal::service('renderer')
                    ->renderPlain($cta_button);
                  break;
              }
            }
          }
        }
      }
    }
  }
}

/**
 * Render a link to an iteration as full link with title or just absolute as string.
 *
 * @param int $iteration_enrollment_id
 *   The ID of the iteration enrollment.
 * @param bool $as_link
 *   True if you want the full link otherwise just a canonical URL string.
 *
 * @return string|null
 *   the Url.
 *
 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 * @throws \Drupal\Core\Entity\EntityMalformedException
 */
function _social_lms_integrator_iteration_get_link_to_iteration_from_enrollment(int $iteration_enrollment_id, bool $as_link = FALSE) {
  // Check if the iteration still exists.
  if ($iteration = _social_lms_integrator_iteration_get_iteration_from_enrollment($iteration_enrollment_id)) {
    if ($as_link) {
      $url_string = $iteration->toUrl('canonical')->toString(TRUE);
      return $url_string->getGeneratedUrl();
    }
    else {
      return Link::fromTextAndUrl($iteration->getTitle(), $iteration->toUrl('canonical'))->toString();
    }
  }
  else {
    return NULL;
  }
}

/**
 * Get the user display name from an Enrollment also for AN.
 *
 * @param int $id
 *   the Enrollment entity ID.
 *
 * @return array|\Drupal\Component\Render\MarkupInterface|mixed|null|string
 *   Markup for the Username.
 *
 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 */
function _social_lms_integrator_iteration_get_name_from_enrollment($id) {
  $entity_storage = \Drupal::entityTypeManager()
    ->getStorage('iteration_enrollment');
  /** @var \Drupal\social_lms_integrator_enrollment\Entity\IterationEnrollment $enrollment */
  $enrollment = $entity_storage->load($id);

  // User is AN by default.
  $enrollment_name = \Drupal::configFactory()->get('user.settings')->get('anonymous');

  if ($enrollment instanceof IterationEnrollmentInterface) {
    // If there is a Uid. Lets load the user and return their display name.
    if ($enrollment !== NULL &&
      $enrollment->hasField('field_account') &&
      $enrollment->getFieldValue('field_account', 'target_id') > 0) {
      $entity_storage = \Drupal::entityTypeManager()
        ->getStorage('user');
      /** @var \Drupal\user\Entity\User $user */
      $user = $entity_storage->load($enrollment->getFieldValue('field_account', 'target_id'));
      return $user->getDisplayName();
    }
  }

  // User is AN but has enrollment settings available, lets see if we can get
  // the Firstname and lastname to show up in the notification.
  if ($enrollment !== NULL &&
    $enrollment->hasField('field_first_name') &&
    $enrollment->getFieldValue('field_first_name', 'value') !== NULL &&
    $enrollment->getFieldValue('field_last_name', 'value') !== NULL) {
    // Create the Name from AN Attendees if possible.
    $enrollment_name = $enrollment->getFieldValue('field_first_name', 'value') .
      ' ' . $enrollment->getFieldValue('field_last_name', 'value');
  }

  return $enrollment_name;
}

/**
 * Gets the iteration object from iteration enrollment ID.
 *
 * @param int $iteration_enrollment_id
 *   Iteration enrollment ID.
 *
 * @return \Drupal\node\Entity\Node|null
 *   Iteration object.
 *
 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 */
function _social_lms_integrator_iteration_get_iteration_from_enrollment(int $iteration_enrollment_id) {
  $entity_storage = \Drupal::entityTypeManager()
    ->getStorage('iteration_enrollment');
  if ($entity = $entity_storage->load($iteration_enrollment_id)) {
    /** @var \Drupal\social_lms_integrator_enrollment\Entity\IterationEnrollment $entity */
    $iteration_id = $entity->getFieldValue('field_iteration', 'target_id');
    $storage = \Drupal::entityTypeManager()->getStorage('node');
    /** @var \Drupal\node\Entity\Node $iteration */
    $iteration = $storage->load($iteration_id);
  }
  return $iteration ?? NULL;
}