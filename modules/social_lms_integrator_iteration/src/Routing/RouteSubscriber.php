<?php

namespace Drupal\social_lms_integrator_iteration\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('view.group_iterations.page_group_iterations')) {
      $route->setRequirement('_social_lms_integrator_iteration_custom_access', 'Drupal\social_lms_integrator_iteration\Access\SocialLMSIntegratorIterationAccessCheck');
    }
  }
}
