<?php

namespace Drupal\social_lms_integrator_iteration;

use Drupal\group\Entity\GroupInterface;
use Drupal\Core\Session\AccountProxy;

/**
 * Interface SocialLMSIntegratorIterationEnrollmentInfoInterface.
 *
 * @package Drupal\social_lms_integrator_iteration
 */
interface SocialLMSIntegratorIterationEnrollmentInfoInterface {

  public function getDefaultIterationRecords(GroupInterface $group);

  public function getEnrolledIterationRecords(GroupInterface $group);

  public function getPendingIterationRecords(GroupInterface $group);

}
