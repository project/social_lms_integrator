<?php

namespace Drupal\social_lms_integrator_iteration_enrollment_notify\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Determines access to for translating Social LMS Integrator iteration enrollment welcome messages.
 */
class IterationWelcomeMessageTranslateConfigAccessCheck implements AccessInterface {

  /**
   * Checks access to the Social LMS Integrator iteration enrollment welcome message translate routes.
   */
  public function access(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, "translate iteration welcome messages");
  }

}
