<?php

namespace Drupal\social_lms_integrator_iteration_enrollment_notify\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Iteration Welcome Message settings.
 */
class IterationWelcomeMessageSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'iteration_welcome_message_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'social_lms_integrator_iteration_enrollment_notify.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $settings = $this->config('social_lms_integrator_iteration_enrollment_notify.settings');


    // Get default message.
    $default_welcome_message = \Drupal::entityTypeManager()->getStorage('iteration_welcome_message')->load('default_iteration_welcome_message');
    if ($default_welcome_message) {      
      $default_subject = $default_welcome_message->getSubject();
      $default_body = $default_welcome_message->getBody()['value'];
      $default_body_existing = $default_welcome_message->getBodyExisting()['value'];
    }  

    $available_filters = filter_formats();

    foreach($available_filters as $id => $filter) {
      $filter_options[$id] = $filter->label();
    }


    $form['format_settings'] = [
      '#type'  => 'fieldset',
      '#title' => $this->t('Format settings'),
    ];

    $form['format_settings']['selected_format'] = [
      '#type' => 'select',
      '#options' => $filter_options,
      '#default_value' => $settings->get('selected_format'),
    ];

    $form['format_settings']['show_token_info'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Show token info'),
      '#default_value' => $settings->get('show_token_info'),
      '#description' => $this->t('If enabled a bok with available tokens are shown to users.')
    ];

    $selected_format = $settings->get('selected_format');    

    $form['message'] = [
      '#type'          => 'fieldset',
      '#title'         => $this->t('Default message'),
      '#description'   => $this->t('If a default message is set here, and no message is set for the group, this default message will be sent.'),
    ];

    $form['message']['default_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#maxlength' => 255,
      '#default_value' => isset($default_subject) ? $default_subject : ''
    ];
 
    $form['message']['default_body'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Body'),
      '#default_value' => isset($default_body) ? $default_body : '',
      '#format' => $selected_format,
      '#allowed_formats' => [
        $selected_format
      ]
    ];    

    $form['message']['default_body_existing'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Body existing'),
      '#default_value' => isset($default_body_existing) ? $default_body_existing : '',
      '#required' => FALSE,
      '#format' => $selected_format,
      '#allowed_formats' => [
        $selected_format
      ]
    ];  

    $form['message']['available_tokens'] = array(
      '#type' => 'details',
      '#title' => t('Available Tokens'),
      '#open' => FALSE, // Controls the HTML5 'open' attribute. Defaults to FALSE.
    );

    $suppported_tokens = array('site','user','group','node');
    
    $available_field_tokens = \Drupal::service('social_lms_integrator_iteration_enrollment_notify.available_fields');
    $whitelist = $available_field_tokens->getAvailableFields();

    $options = [
      'show_restricted' => TRUE,
      'show_nested' => TRUE,
      'global_types' => FALSE,
      'whitelist' => $whitelist
    ];  

    $form['message']['available_tokens']['tokens'] = \Drupal::service('social_lms_integrator_iteration_enrollment_notify.tree_builder')
    ->buildRenderable($suppported_tokens,$options);


    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $settings = $this->configFactory->getEditable('social_lms_integrator_iteration_enrollment_notify.settings');

    // Save a default welcome message
    $default_welcome_message = \Drupal::entityTypeManager()->getStorage('iteration_welcome_message')->load('default_iteration_welcome_message');
    if (!$default_welcome_message) {        
      $default_welcome_message = \Drupal::entityTypeManager()->getStorage('iteration_welcome_message')->create();
    }     

    $default_welcome_message->set('id', 'default_iteration_welcome_message');
    $default_welcome_message->set('label', 'default_iteration_welcome_message');
    $default_welcome_message->set('node', '0');
    $default_welcome_message->setSubject($form_state->getValue('default_subject'));
    $default_welcome_message->setBody($form_state->getValue('default_body'));
    $default_welcome_message->setBodyExisting($form_state->getValue('default_body_existing'));
    $default_welcome_message->save();       

    // Save configurations.
    $settings->set('selected_format', $form_state->getValue('selected_format'))->save();
    $settings->set('show_token_info', $form_state->getValue('show_token_info'))->save();

    \Drupal::messenger()->addMessage(t('Configuration saved.'));

  }

}

