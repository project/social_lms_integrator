<?php

namespace Drupal\social_lms_integrator_iteration_managers\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views_bulk_operations\Form\ConfigureAction;

/**
 * Action configuration form.
 */
class SocialLMSIntegratorIterationManagersIterationApplicationViewsBulkOperationsConfigureAction extends ConfigureAction {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $view_id = 'iteration_application', $display_id = 'page_application') {
    return parent::buildForm($form, $form_state, 'iteration_application', 'page_application');
  }

}
