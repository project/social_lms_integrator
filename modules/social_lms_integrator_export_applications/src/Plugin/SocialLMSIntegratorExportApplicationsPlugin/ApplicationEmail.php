<?php

namespace Drupal\social_lms_integrator_export_applications\Plugin\SocialLMSIntegratorExportApplicationsPlugin;

use Drupal\social_lms_integrator_export_applications\Plugin\SocialLMSIntegratorExportApplicationsPluginBase;
use Drupal\social_lms_integrator_application\ApplicationInterface;

/**
 * Provides a 'ApplicationEmail' Social LMS Integrator export applications row.
 *
 * @SocialLMSIntegratorExportApplicationsPlugin(
 *  id = "application_email",
 *  label = @Translation("Email"),
 *  weight = -480,
 * )
 */
class ApplicationEmail extends SocialLMSIntegratorExportApplicationsPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getHeader() {
    return $this->t('Email');
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(ApplicationInterface $entity) {
    // Get the user from the Application entity
    $user = $this->getAccount($entity);
    return $user->getEmail();
  }

}