<?php

namespace Drupal\social_lms_integrator_export_applications\Plugin\SocialLMSIntegratorExportApplicationsPlugin;

use Drupal\social_lms_integrator_export_applications\Plugin\SocialLMSIntegratorExportApplicationsPluginBase;
use Drupal\social_lms_integrator_application\ApplicationInterface;

/**
 * Provides a 'ApplicationAddressLine1' Social LMS Integrator export application row.
 *
 * @SocialLMSIntegratorExportPlugin(
 *  id = "application_address_line1",
 *  label = @Translation("Address line 1"),
 *  weight = -480,
 * )
 */
class ApplicationAddressLine1 extends SocialLMSIntegratorExportPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getHeader() {
    return $this->t('Address line 1');
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(ApplicationInterface $entity) {
    // Get the user from the Application entity
    $user = $this->getAccount($entity);
    return $this->profileGetAddressFieldValue('field_profile_address', 'address_line1', $this->getProfile($user));
  }

}