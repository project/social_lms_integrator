<?php

namespace Drupal\social_lms_integrator_export_applications\Plugin\SocialLMSIntegratorExportApplicationsPlugin;

use Drupal\social_lms_integrator_export_applications\Plugin\SocialLMSIntegratorExportApplicationsPluginBase;
use Drupal\social_lms_integrator_application\ApplicationInterface;

/**
 * Provides a 'ApplicationAddressAdministrative' Social LMS Integrator export applications row.
 *
 * @SocialLMSIntegratorExportApplicationsPlugin(
 *  id = "application_address_administrative",
 *  label = @Translation("Administrative address"),
 *  weight = -480,
 * )
 */
class ApplicationAddressAdministrative extends SocialLMSIntegratorExportApplicationsPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getHeader() {
    return $this->t('Administrative address');
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(ApplicationInterface $entity) {
    // Get the user from the Application entity
    $user = $this->getAccount($entity);
    return $this->profileGetAddressFieldValue('field_profile_address', 'administrative_area', $this->getProfile($user));
  }

}