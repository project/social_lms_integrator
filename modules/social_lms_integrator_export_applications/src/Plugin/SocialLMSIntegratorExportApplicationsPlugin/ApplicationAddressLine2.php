<?php

namespace Drupal\social_lms_integrator_export_applications\Plugin\SocialLMSIntegratorExportPlugin;

use Drupal\social_lms_integrator_export_applications\Plugin\SocialLMSIntegratorExportApplicationsPluginBase;
use Drupal\social_lms_integrator_application\ApplicationInterface;

/**
 * Provides a 'ApplicationAddressLine2' Social LMS Integrator export application row.
 *
 * @SocialLMSIntegratorExportApplicationPlugin(
 *  id = "application_address_line2",
 *  label = @Translation("Address line 2"),
 *  weight = -480,
 * )
 */
class ApplicationAddressLine2 extends SocialLMSIntegratorExportApplicationsPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getHeader() {
    return $this->t('Address line 2');
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(ApplicationInterface $entity) {
    // Get the user from the Application entity
    $user = $this->getAccount($entity);
    return $this->profileGetAddressFieldValue('field_profile_address', 'address_line2', $this->getProfile($user));
  }

}