<?php

namespace Drupal\social_lms_integrator_export_applications\Plugin\SocialLMSIntegratorExportApplicationsPlugin;

use Drupal\social_lms_integrator_export_applications\Plugin\SocialLMSIntegratorExportApplicationsPluginBase;
use Drupal\social_lms_integrator_application\ApplicationInterface;
use Drupal\user\UserInterface;

/**
 * Provides a 'ApplicationSupervisor' Social LMS Integrator export applications row.
 *
 * @SocialLMSIntegratorExportApplicationsPlugin(
 *  id = "application_supervisor",
 *  label = @Translation("Supervisor"),
 *  weight = -460,
 * )
 */
class ApplicationSupervisor extends SocialLMSIntegratorExportApplicationsPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getHeader() {
    return $this->t('Supervisor');
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(ApplicationInterface $entity) {
    $name = '';
    $user = $entity->field_supervisor->entity;
    if ($user instanceOf UserInterface) {
      $first_name = $this->profileGetFieldValue('field_profile_first_name', $this->getProfile($user));
      $last_name = $this->profileGetFieldValue('field_profile_last_name', $this->getProfile($user));
      if (isset($first_name) && !empty($first_name)) {
        $name = $first_name;
      }
      if (isset($last_name) && !empty($last_name)) {
        $name = $name . ' ' . $last_name;
      }
    }  
    
    return $name;
    
  }


}
