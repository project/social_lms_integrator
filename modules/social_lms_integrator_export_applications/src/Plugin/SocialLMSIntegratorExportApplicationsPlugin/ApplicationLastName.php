<?php

namespace Drupal\social_lms_integrator_export_applications\Plugin\SocialLMSIntegratorExportApplicationsPlugin;

use Drupal\social_lms_integrator_export_applications\Plugin\SocialLMSIntegratorExportApplicationsPluginBase;
use Drupal\social_lms_integrator_application\ApplicationInterface;

/**
 * Provides a 'UserLastName' Social LMS Integrator export applications row.
 *
 * @SocialLMSIntegratorExportApplicationsPlugin(
 *  id = "application_last_name",
 *  label = @Translation("Last name"),
 *  weight = -470,
 * )
 */
class ApplicationLastName extends SocialLMSIntegratorExportApplicationsPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getHeader() {
    return $this->t('Last name');
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(ApplicationInterface $entity) {
    // Get the user from the Application entity
    $user = $this->getAccount($entity);
    return $this->profileGetFieldValue('field_profile_last_name', $this->getProfile($user));
  }

}