<?php

namespace Drupal\social_lms_integrator_export_applications\Plugin\SocialLMSIntegratorExportApplicationsPlugin;

use Drupal\social_lms_integrator_export_applications\Plugin\SocialLMSIntegratorExportApplicationsPluginBase;
use Drupal\social_lms_integrator_application\ApplicationInterface;

/**
 * Provides a 'ApplicationAppliedOn' Social LMS Integrator export applications row.
 *
 * @SocialLMSIntegratorExportApplicationsPlugin(
 *  id = "application_applied_on",
 *  label = @Translation("Applied on"),
 *  weight = -460,
 * )
 */
class ApplicationAppliedOn extends SocialLMSIntegratorExportApplicationsPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getHeader() {
    return $this->t('Applied on');
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(ApplicationInterface $entity) {
    return $this->format($entity);
  }

  /**
   * Formats a date, using a date type or a custom date format string.
   *
   * @param mixed $entity
   *   The entity object.
   *
   * @return string
   *   A translated date string in the requested format. Since the format may
   *   contain user input, this value should be escaped when output.
   */
  public function format($entity) {
    return $this->dateFormatter->format($entity->getCreatedTime(), 'short');
  }

}