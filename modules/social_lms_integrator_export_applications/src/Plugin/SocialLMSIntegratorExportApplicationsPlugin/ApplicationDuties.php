<?php

namespace Drupal\social_lms_integrator_export_applications\Plugin\SocialLMSIntegratorExportApplicationsPlugin;

use Drupal\social_lms_integrator_export_applications\Plugin\SocialLMSIntegratorExportApplicationsPluginBase;
use Drupal\social_lms_integrator_application\ApplicationInterface;

/**
 * Provides a 'ApplicationDuties' Social LMS Integrator export applications row.
 *
 * @SocialLMSIntegratorExportApplicationsPlugin(
 *  id = "application_duties",
 *  label = @Translation("Duties"),
 *  weight = -460,
 * )
 */
class ApplicationDuties extends SocialLMSIntegratorExportApplicationsPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getHeader() {
    return $this->t('Duties');
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(ApplicationInterface $entity) {
    return $entity->field_duties->value;
  }


}