<?php

namespace Drupal\social_lms_integrator_export_applications\Plugin\SocialLMSIntegratorExportApplicationsPlugin;

use Drupal\social_lms_integrator_export_applications\Plugin\SocialLMSIntegratorExportApplicationsPluginBase;
use Drupal\social_lms_integrator_application\ApplicationInterface;

/**
 * Provides a 'ApplicationOrganization' Social LMS Integrator export applications row.
 *
 * @SocialLMSIntegratorExportApplicationsPlugin(
 *  id = "application_organization",
 *  label = @Translation("Organization"),
 *  weight = -320,
 * )
 */
class ApplicationOrganization extends SocialLMSIntegratorExportApplicationsPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getHeader() {
    return $this->t('Organization');
  }

  /**
   * Returns the value.
   *
   * @param \Drupal\social_lms_integrator_application\ApplicationInterface $entity
   *   The Application entity to get the value from.
   *
   * @return string
   *   The value.
   */
  public function getValue(ApplicationInterface $entity) {
    $user = $this->getAccount($entity);
    return $this->profileGetFieldValue('field_profile_organization', $this->getProfile($user));
  }

}
