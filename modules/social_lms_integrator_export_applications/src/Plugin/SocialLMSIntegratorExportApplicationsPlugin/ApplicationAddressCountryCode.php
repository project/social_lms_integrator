<?php

namespace Drupal\social_lms_integrator_export_applications\Plugin\SocialLMSIntegratorExportApplicationsPlugin;

use Drupal\social_lms_integrator_export_applications\Plugin\SocialLMSIntegratorExportApplicationsPluginBase;
use Drupal\social_lms_integrator_application\ApplicationInterface;

/**
 * Provides a 'ApplicationAddressCountryCode' Social LMS Integrator export applications row.
 *
 * @SocialLMSIntegratorExportApplicationsPlugin(
 *  id = "application_address_country_code",
 *  label = @Translation("Country code"),
 *  weight = -480,
 * )
 */
class ApplicationAddressCountryCode extends SocialLMSIntegratorExportApplicationsPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getHeader() {
    return $this->t('Country code');
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(ApplicationInterface $entity) {
    // Get the user from the Application entity
    $user = $this->getAccount($entity);
    return $this->profileGetAddressFieldValue('field_profile_address', 'country_code', $this->getProfile($user));
  }

}