<?php

namespace Drupal\social_lms_integrator_export_applications\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Social LMS Integrator export applications plugin item annotation object.
 *
 * @see \Drupal\social_lms_integrator_export_applications\Plugin\SocialLMSIntegratorExportApplicationsPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class SocialLMSIntegratorExportApplicationsPlugin extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The plugin weight.
   *
   * @var int
   */
  public $weight;

}
