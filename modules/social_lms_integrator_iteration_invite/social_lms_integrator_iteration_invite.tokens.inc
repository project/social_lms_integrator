<?php

/**
 * @file
 * Builds placeholder replacement tokens for Social LMS Integrator Iteration AN Enroll module.
 */

use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Url;
use Drupal\message\Entity\Message;
use Drupal\user\Entity\User;

/**
 * Implements hook_token_info().
 */
function social_lms_integrator_iteration_invite_token_info() {
  $type = [
    'name' => t('Social LMS Integrator Iteration Invite'),
    'description' => t('Tokens from the Social LMS Integrator Iteration Invite module.'),
  ];

  $invite['iteration_register_link'] = [
    'name' => t('Register Link'),
    'description' => t('Url to register page with prefilled email address.'),
  ];

  $invite['user_login_iteration_destination'] = [
    'name' => t('Iteration link with login page'),
    'description' => t('Url to the login page with iteration as destination.'),
  ];

  $invite['user_login_iteration_invites_overview'] = [
    'name' => t('Iteration link with login page'),
    'description' => t('Url to the login page with iteration as destination.'),
  ];

  return [
    'types' => ['social_lms_integrator_iteration_invite' => $type],
    'tokens' => [
      'social_lms_integrator_iteration_invite' => $invite,
    ],
  ];
}

/**
 * Implements hook_tokens().
 */
function social_lms_integrator_iteration_invite_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];
  if ($type == 'social_lms_integrator_iteration_invite') {
    foreach ($tokens as $name => $original) {
      if (
        !empty($data['node']) &&
        $name == 'iteration_register_link' &&
        !empty($data['iteration_enrollment'])
      ) {
        $iteration = $data['node'];

        // Load the current Iteration enrollments so we can check duplicates.
        $storage = \Drupal::entityTypeManager()->getStorage('iteration_enrollment');
        /** @var \Drupal\social_lms_integrator_enrollment\IterationEnrollmentInterface $iteration_enrollment */
        $iteration_enrollment = $storage->load($data['iteration_enrollment']);

        $mail = $iteration_enrollment->get('field_email')->getString();
        if (!$iteration_enrollment->get('field_account')->isEmpty()) {
          /** @var \Drupal\user\UserInterface $user */
          $user = \Drupal::entityTypeManager()->getStorage('user')
            ->load($iteration_enrollment->get('field_account')->getString());
          $mail = $user->getEmail();
        }
        $mail_encoded = str_replace(['+', '/', '='], [
          '-',
          '_',
          '',
        ], base64_encode((string) $mail));

        $destination = Url::fromRoute('entity.node.canonical', ['node' => $iteration->id()])->toString();

        $options = [
          'invitee_mail' => $mail_encoded,
          'destination' => $destination,
        ];

        $register_link = Url::fromRoute('user.register', $options, ['absolute' => TRUE])
          ->toString();
        $replacements[$original] = $register_link;
      }
      if ($name == 'user_login_iteration_destination') {
        /** @var \Drupal\message\Entity\Message $message */
        $message = $data['message'] ?? NULL;

        if ($message instanceof Message) {
          // Load the current Iteration enrollments so we can check duplicates.
          $storage = \Drupal::entityTypeManager()->getStorage('iteration_enrollment');
          $iteration_enrollment = $storage->load($message->getFieldValue('field_message_related_object', 'target_id'));

          if ($iteration_enrollment) {
            $iteration = $iteration_enrollment->get('field_iteration')->getString();
            $destination = Url::fromRoute('entity.node.canonical', ['node' => $iteration])
              ->toString();
            $login_link = Url::fromRoute('user.login', ['destination' => $destination], ['absolute' => TRUE])
              ->toString();
            $replacements[$original] = $login_link;
          }
        }
      }
      if ($name == 'user_login_iteration_invites_overview') {
        /** @var \Drupal\message\Entity\Message $message */
        $message = $data['message'] ?? NULL;

        if ($message instanceof Message) {
          // Load the current Iteration enrollments so we can check duplicates.
          $storage = \Drupal::entityTypeManager()->getStorage('iteration_enrollment');
          $iteration_enrollment = $storage->load($message->getFieldValue('field_message_related_object', 'target_id'));

          if ($iteration_enrollment) {
            $user = User::load($iteration_enrollment->get('field_account')->getString());
            $my_invitations_link = Url::fromRoute('view.user_iteration_invites.page_user_iteration_invites', ['user' => $user->id()], ['absolute' => TRUE])->toString();
            $replacements[$original] = $my_invitations_link;
          }
        }
      }
    }
  }
  return $replacements;
}