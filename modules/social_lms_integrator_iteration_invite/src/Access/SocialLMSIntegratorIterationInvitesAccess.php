<?php

namespace Drupal\social_lms_integrator_iteration_invite\Access;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Access\AccessResult;
use Drupal\social_lms_integrator_iteration_invite\SocialLMSIntegratorIterationInviteAccessHelper;

/**
 * Class SocialLMSIntegratorIterationInvitesAccess.
 *
 * @package Drupal\social_lms_integrator_iteration_invite\Access
 */
class SocialLMSIntegratorIterationInvitesAccess {

  /**
   * The iteration invite access helper.
   *
   * @var \Drupal\social_lms_integrator_iteration_invite\SocialLMSIntegratorIterationInviteAccessHelper
   */
  protected $accessHelper;

  /**
   * IterationInvitesAccess constructor.
   *
   * @param \Drupal\social_lms_integrator_iteration_invite\SocialLMSIntegratorIterationInviteAccessHelper $accessHelper
   *   The iteration invite access helper.
   */
  public function __construct(SocialLMSIntegratorIterationInviteAccessHelper $accessHelper) {
    $this->accessHelper = $accessHelper;
  }

  /**
   * Custom access check on the invite features on iterations.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   Returns the result of the access helper.
   *
   * @see \Drupal\social_lms_integrator_iteration_invite\SocialLMSIntegratorIterationInviteAccessHelper::iterationFeatureAccess()
   */
  public function iterationFeatureAccess() {
    try {
      return $this->accessHelper->iterationFeatureAccess();
    }
    catch (InvalidPluginDefinitionException $e) {
      return AccessResult::neutral();
    }
    catch (PluginNotFoundException $e) {
      return AccessResult::neutral();
    }
  }

  /**
   * Custom access check for the user invite overview.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   Returns the result of the access helper.
   *
   * @see \Drupal\social_lms_integrator_iteration_invite\SocialLMSIntegratorIterationInviteAccessHelper::userInviteAccess()
   */
  public function userInviteAccess() {
    return $this->accessHelper->userInviteAccess();
  }

}
