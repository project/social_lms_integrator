<?php

namespace Drupal\social_lms_integrator_iteration_enrollments_invite\Plugin\Action;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\social_lms_integrator_enrollment\IterationEnrollmentInterface;
use Drupal\social_lms_integrator_iteration_enrollments_req\Plugin\Action\ExportIterationEnrollmentsRequests;

/**
 * Exports an iteration enrollment invites accounts to CSV.
 *
 * @Action(
 *   id = "social_lms_integrator_iteration_enrollments_invite_export_enrollments_action",
 *   label = @Translation("Export the selected iteration enrollments invites to CSV"),
 *   type = "iteration_enrollment",
 *   confirm = TRUE,
 *   confirm_form_route_name = "social_lms_integrator_iteration_managers.invite.vbo.confirm"
 * )
 */
class ExportIterationEnrollmentsInvites extends ExportIterationEnrollmentsRequests {

  /**
   * {@inheritdoc}
   */
  public function executeMultiple(array $entities) {
    /** @var \Drupal\social_lms_integrator_enrollment\IterationEnrollmentInterface $entity */
    parent::executeMultiple($entities);
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    if ($object instanceof IterationEnrollmentInterface) {
      $access = $this->getAccount($object)->access('view', $account, TRUE);
    }
    else {
      $access = AccessResult::forbidden();
    }

    return $return_as_object ? $access : $access->isAllowed();
  }

  /**
   * {@inheritdoc}
   *
   * To make sure the file can be downloaded, the path must be declared in the
   * download pattern of the social user export module.
   *
   * @see social_user_export_file_download()
   */
  protected function generateFilePath() : string {
    $hash = md5(microtime(TRUE));
    return 'export-iteration-enrollment-invites-' . substr($hash, 20, 12) . '.csv';
  }

  /**
   * Extract user entity from event enrollment entity.
   *
   * @param \Drupal\social_lms_integrator_enrollment\IterationEnrollmentInterface $entity
   *   The iteration enrollment.
   *
   * @return \Drupal\user\UserInterface
   *   The user.
   */
  public function getAccount(IterationEnrollmentInterface $entity) {
    $accounts = $entity->field_account->referencedEntities();
    return reset($accounts);
  }

}
