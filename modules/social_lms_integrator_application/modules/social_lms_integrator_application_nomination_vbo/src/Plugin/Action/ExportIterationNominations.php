<?php

namespace Drupal\social_lms_integrator_application_nomination_vbo\Plugin\Action;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\social_lms_integrator_application\ApplicationInterface;
use Drupal\social_lms_integrator_application_vbo\Plugin\Action\ExportIterationApplications;

/**
 * Exports an iteration nominations accounts to CSV.
 *
 * @Action(
 *   id = "social_lms_integrator_nomination_vbo_export_nominations_action",
 *   label = @Translation("Export the selected iteration nominations to CSV"),
 *   type = "application",
 *   confirm = TRUE,
 *   confirm_form_route_name = "social_lms_integrator_iteration_managers.nomination.vbo.confirm"
 * )
 */
class ExportIterationNominations extends ExportIterationApplications {

  /**
   * {@inheritdoc}
   */
  public function executeMultiple(array $entities) {
    /** @var \Drupal\social_lms_integrator_application\ApplicationInterface $entity */
    parent::executeMultiple($entities);
  }


  /**
   * {@inheritdoc}
   *
   * To make sure the file can be downloaded, the path must be declared in the
   * download pattern of the social user export module.
   *
   * @see social_user_export_file_download()
   */
  protected function generateFilePath() : string {
    $hash = md5(microtime(TRUE));
    return 'export-iteration-nominations-' . substr($hash, 20, 12) . '.csv';
  }



}
