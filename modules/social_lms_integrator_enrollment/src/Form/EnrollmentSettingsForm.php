<?php

namespace Drupal\social_lms_integrator_enrollment\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\GroupType;
use Drupal\Component\Utility\UrlHelper;

/**
 * Class EnrollmentSettingsForm.
 */
class EnrollmentSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'enrollment_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $social_lms_integrator_enrollment_config = $this->configFactory->getEditable('social_lms_integrator_enrollment.settings');

    if ($social_lms_integrator_enrollment_config && $social_lms_integrator_enrollment_config->get('url_type')) {
      $url_type = $social_lms_integrator_enrollment_config->get('url_type');
    }

    $url_options = [
      'internal' => 'internal',
      'external' => 'external',
    ];

    $form['url_options'] = [
      '#type' => 'radios',
      '#options' => $url_options,
      '#default_value' => isset($url_type) ? $url_type : 'internal',
    ];

    $form['redirect_url_internal'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Redirect URL'),
      '#default_value' => $social_lms_integrator_enrollment_config->get('redirect_url'),
      '#description' => $this->t('Enter a relative path for your lms beginning with /'),
      '#states' => [
        'visible' => [
          ':input[name="url_options"]' => array('value' => 'internal'),
        ]
      ]
    ];

    $form['redirect_url_external'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Redirect URL'),
      '#default_value' => $social_lms_integrator_enrollment_config->get('redirect_url'),
      '#description' => $this->t('Enter an external url for your lms.'),
      '#states' => [
        'visible' => [
          ':input[name="url_options"]' => array('value' => 'external'),
        ]
      ]
    ];



    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#button_level' => 'raised',
      '#value' => $this->t('Save configuration'),
    ];

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $host = \Drupal::request()->getSchemeAndHttpHost();
    $url_options = $form_state->getValue('url_options');

    $redirect_url_internal = $form_state->getValue('redirect_url_internal');
    $redirect_url_external = $form_state->getValue('redirect_url_external');

    if (isset($url_options) && !empty($url_options)) {

      if ($url_options === 'internal') {
        if (!empty($redirect_url_internal) && 
        str_starts_with($redirect_url_internal, '/') == FALSE
      ) {
        $form_state->setErrorByName('redirect_url_internal', t('You must enter a valid path.'));
        }
      }

      if ($url_options === 'external') {
        if (!empty($redirect_url_external) && 
        UrlHelper::isValid($redirect_url_external) == FALSE
      ) {
        $form_state->setErrorByName('redirect_url_external', t('You must enter a valid url.'));
        }
      }


    }




  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('social_lms_integrator_enrollment.settings');
    $url_option = $form_state->getValue('url_options');

    if ($url_option === 'internal') {
      $url = $form_state->getValue('redirect_url_internal');
    }
    else {
      $url = $form_state->getValue('redirect_url_external');
    }
   
    
    $config->set('redirect_url', $url);
    $config->set('url_type', $url_option);
    $config->save();

  }

  /**
   * Gets the configuration names that will be editable.
   */
  protected function getEditableConfigNames() {
    // @todo Implement getEditableConfigNames() method.
  }

}
