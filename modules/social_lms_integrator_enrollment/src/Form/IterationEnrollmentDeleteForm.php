<?php

namespace Drupal\social_lms_integrator_enrollment\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Iteration enrollment entities.
 *
 * @ingroup social_lms_integrator_enrollment
 */
class IterationEnrollmentDeleteForm extends ContentEntityDeleteForm {

}
