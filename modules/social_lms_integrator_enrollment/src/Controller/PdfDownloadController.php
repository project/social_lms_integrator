<?php

namespace Drupal\social_lms_integrator_enrollment\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

/**
 * Class PdfDownloadController.
 */
class PdfDownloadController extends ControllerBase {

  public function download() {

    $uri_prefix = 'public://downloads/';
    
    $filename = 'supervisor_confirmation_form.pdf';

    $uri = $uri_prefix . $filename;

    $headers = [
      'Content-Type' => 'application/pdf', // Would want a condition to check for extension and set Content-Type dynamically
      'Content-Description' => 'File Download',
      'Content-Disposition' => 'attachment; filename=' . $filename
    ];

    // Return and trigger file donwload.
    return new BinaryFileResponse($uri, 200, $headers, true );

  }
}
